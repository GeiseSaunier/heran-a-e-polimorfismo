
#ifndef HEXAGONO_HPP
#define HEXAGONO_HPP
#include <iostream>
#include "formageometrica.hpp"

 class Hexagono: public FormaGeometrica {

    private:

        float lado;

    public:

     Hexagono();
    ~Hexagono();

    void set_lado (float lado);
    float get_lado ();

    float calcula_area();
    float calcula_perimetro();

};

#endif