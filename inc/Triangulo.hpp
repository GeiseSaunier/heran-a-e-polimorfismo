
#ifndef TRIANGULO_HPP
#define TRIANGULO_HPP
#include "formageometrica.hpp"
#include <iostream>

using namespace std;

class Triangulo: public FormaGeometrica {

    private:

    float lado1, lado2, lado3, semiperimetro;


    public:

    Triangulo();
    ~Triangulo();

    void set_lado1 (float lado1);
    float get_lado1 ();

    void set_lado2 (float lado2);
    float get_lado2 ();

    void set_lado3 (float lado3);
    float get_lado3 ();

    void set_semiperimetro (float semiperimetro);
    float get_semiperimetro ();

    float calcula_area();
    float calcula_perimetro();

};

#endif
