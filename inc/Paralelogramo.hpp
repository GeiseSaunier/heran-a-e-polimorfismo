
#ifndef PARALELOGRAMO_HPP
#define PARALELOGRAMO_HPP
#include "formageometrica.hpp"
#include <iostream>

class Paralelogramo: public FormaGeometrica {

    private:

        float lado1;
        float lado2;

    public:

    Paralelogramo();
    ~Paralelogramo();

    void set_lado1 (float lado1);
    float get_lado1 ();

    void set_lado2 (float lado2);
    float get_lado2 ();

    float calcula_area( );
    float calcula_perimetro();


};

#endif