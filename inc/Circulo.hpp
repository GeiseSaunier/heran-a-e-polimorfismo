
#ifndef CIRCULO_HPP
#define CIRCULO_HPP
#include "formageometrica.hpp"
#include <iostream>

 class Circulo: public FormaGeometrica {

private:

    float raio;
    float pi;


public: 


    Circulo ();
    ~Circulo ();

    void set_raio (float raio);
    float get_raio ();

    void set_pi (float pi);
    float get_pi ();

    float calcula_area();
    float calcula_perimetro();

};

#endif