
#ifndef PENTAGONO_HPP
#define PENTAGONO_HPP
#include "formageometrica.hpp"
#include <iostream>

class Pentagono: public FormaGeometrica {

private:

   
    float apotema;
    float lado;
    float perimetro;

public:

    Pentagono();
    ~Pentagono();

    void set_apotema (float apotema);
    float get_apotema();

    void set_lado (float lado);
    float get_lado();

    void set_perimetro(float perimetro);
    float get_perimetro();

    float calcula_area();
    float calcula_perimetro();


};

#endif
