
#ifndef QUADRADO_HPP
#define QUADRADO_HPP
#include "formageometrica.hpp"
#include <iostream>

using namespace std;

class Quadrado: public FormaGeometrica {

    private:
  
        float lado;

    public:

     Quadrado();
    ~Quadrado();

    void set_lado (float lado);
    float get_lado ();

    float calcula_area();
    float calcula_perimetro();

};

#endif