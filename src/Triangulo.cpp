
#include <iostream>
#include "Triangulo.hpp"
#include <math.h>
#include <string>


using namespace std;

Triangulo::Triangulo() {

    set_lado1(9.0f);
    set_lado2(7.0f);
    set_lado3(3.0f);
    set_semiperimetro(9.5f);
    set_tipo("TRIÂNGULO");

}

Triangulo::~Triangulo(){

}

    void Triangulo::set_lado1 (float lado1){
        this -> lado1 = lado1;
    }

    float Triangulo::get_lado1 (){
        return lado1;
    }

     void Triangulo::set_lado2 (float lado2){
        this -> lado2 = lado2;
    }

    float Triangulo::get_lado2 (){
        return lado2;
    }

     void Triangulo::set_lado3 (float lado3){
        this -> lado3 = lado3;
    }

    float Triangulo::get_lado3 (){
        return lado3;
    }

    void Triangulo::set_semiperimetro (float semiperimetro){
        this -> semiperimetro = semiperimetro;
    }

    float Triangulo::get_semiperimetro (){
        return semiperimetro;
    }
    
    float Triangulo::calcula_perimetro(){
    float lado1 = get_lado1();
    float lado2 = get_lado2();
    float lado3 = get_lado3();
    return lado1 + lado2 + lado3;

    }
    float Triangulo::calcula_area(){ // calaculando a área a partir da lei de Heron
    float lado1 = get_lado1();
    float lado2 = get_lado2();
    float lado3 = get_lado3();
    semiperimetro =  ( lado1 + lado2 + lado3 ) / 2;
    return sqrt (semiperimetro * ( semiperimetro - lado1) * (semiperimetro - lado2) * (semiperimetro - lado3));
  
   
        
}





