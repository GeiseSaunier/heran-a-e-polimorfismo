
#include <iostream>
#include "Pentagono.hpp"

using namespace std;

Pentagono::Pentagono() {

    set_apotema(2.0f);
    set_lado(4.0f);
    set_tipo ("PENTAGONO");
    set_perimetro(10.0f);
}

Pentagono::~Pentagono () {

}

void Pentagono::set_apotema(float apotema){
    this -> apotema = apotema;
} 

float Pentagono::get_apotema() {
    return this->apotema;
}

void Pentagono::set_lado(float lado){
    this -> lado = lado;
} 

float Pentagono::get_lado() {
    return this->lado;
}

void Pentagono::set_perimetro (float perimetro){
    this -> perimetro = perimetro;
}

float Pentagono::get_perimetro(){
    return this-> perimetro;
}



float Pentagono::calcula_area(){

    float apotema = get_apotema();
    float perimetro = get_perimetro();
    return (apotema * perimetro) / 2;
 
}
float Pentagono::calcula_perimetro(){
    float lado = get_lado();
    return lado * 5;
}


 
