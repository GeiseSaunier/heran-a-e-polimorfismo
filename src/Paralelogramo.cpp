
#include <iostream>
#include "Paralelogramo.hpp"

Paralelogramo::Paralelogramo () {

    set_lado1(4.0f);
    set_lado2(6.0f);
    set_tipo("PARALELOGRAMO");
}

Paralelogramo::~Paralelogramo() {
}

void Paralelogramo::set_lado1 (float lado1){
    this -> lado1 = lado1;
} 

float Paralelogramo::get_lado1 () {
    return this->lado1;
}

void Paralelogramo::set_lado2 (float lado2){
    this -> lado2 = lado2;
} 

float Paralelogramo::get_lado2 () {
    return this->lado2;
}

float Paralelogramo::calcula_area(){
    float base = get_base();
    float altura = get_altura();
    return  base * altura;
       
   }
float Paralelogramo::calcula_perimetro(){

    float lado1 = get_lado1();
    float lado2 = get_lado2();
    return 2 * ( lado1 + lado2);

    }



