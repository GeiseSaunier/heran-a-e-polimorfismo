
#include <iostream>
#include "Quadrado.hpp"
#include <math.h>

using namespace std;

Quadrado::Quadrado() {
    set_tipo("QUADRADO");
    set_lado(4.0f);
}

Quadrado::~Quadrado(){
}

void Quadrado::set_lado(float lado){
    this -> lado = lado;
}

float Quadrado::get_lado() {
    return this->lado;
}

float Quadrado::calcula_area(){
        float lado = get_lado();
        return lado * lado;
    
 }

float Quadrado::calcula_perimetro(){
    float lado = get_lado();
    return 4 * lado;
    
}
