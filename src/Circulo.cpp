
#include <iostream>
#include "Circulo.hpp"

using namespace std;

Circulo::Circulo () {

    set_raio(2.0f);
    set_pi(3.14);

}

Circulo::~Circulo() {

}

void Circulo::set_raio (float raio){
    this -> raio = raio;
} 

float Circulo::get_raio () {
    return this->raio;
}

void Circulo::set_pi (float pi){
    this -> pi = pi;
} 

float Circulo::get_pi () {
    return this->pi;
}

float Circulo::calcula_area(){

    float pi = get_pi();
    float raio = get_raio();

    return pi * ( raio * raio);
}
float Circulo::calcula_perimetro(){

    float pi = get_pi();
    float raio = get_raio();

    return 2 * pi * raio;
   
}
