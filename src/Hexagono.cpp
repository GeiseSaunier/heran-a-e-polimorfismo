
#include <iostream>
#include "Hexagono.hpp"
#include <math.h>

Hexagono::Hexagono() {

    set_lado(4.0f);
    set_tipo("HEXAGONO");
}

Hexagono::~Hexagono () {

}

void Hexagono::set_lado (float lado){
    this -> lado = lado;
} 

float Hexagono::get_lado () {
    return this->lado;
}

float Hexagono::calcula_area(){
    float lado = get_lado();
    return 6 * (pow (lado,2)) * sqrt(3) / 4; 
        
    }
float Hexagono::calcula_perimetro(){
    float lado = get_lado();
    return  6 * lado;
    
    }
