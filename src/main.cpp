
#include <iostream>
#include "Triangulo.hpp"
#include "Quadrado.hpp"
#include "Circulo.hpp"
#include "Paralelogramo.hpp"
#include "Pentagono.hpp"
#include "Hexagono.hpp"
#include "formageometrica.hpp"
#include <vector>

using namespace std;

int main () {

Triangulo triangulo = Triangulo();

Quadrado quadrado = Quadrado();

Pentagono pentagono = Pentagono();

Paralelogramo paralelogramo = Paralelogramo();

Hexagono hexagono = Hexagono();

Circulo circulo = Circulo();

vector <FormaGeometrica*> formas = {&triangulo, &quadrado, &pentagono, &paralelogramo, &hexagono, &circulo };

int i=0;

for ( i=0; i <formas.size(); i++ ){

cout << formas[i]->get_tipo() << endl;
cout << formas[i]->calcula_area() << endl;
cout << formas[i]->calcula_perimetro() << endl;

}
  
}